<!doctype html>
<html>
<head>
	<title><?php wp_title('«', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

	<?php if(get_header_image()): ?>
		<div class="main-logo">
			<a href="<?php echo site_url(); ?>">
				<img src="<?php header_image(); ?>" alt="main-logo">
			</a>
		</div>
		<!-- /.main-logo -->
		<?php endif;
	?>

	<?php 
		   /**
			* Displays a navigation menu
			* @param array $args Arguments
			*/
			$header_menu_args = array(
				'theme_location' => '',
				'menu' => '',
				'container' => 'nav',
				'container_class' => 'menu-{menu-slug}-container',
				'container_id' => '',
				'menu_class' => 'menu',
				'menu_id' => '',
				'echo' => true,
				'fallback_cb' => 'wp_page_menu',
				'before' => '',
				'after' => '',
				'link_before' => '',
				'link_after' => '',
				'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
				'depth' => 0,
				'walker' => ''
			);
		
			wp_nav_menu( $header_menu_args );
	?>