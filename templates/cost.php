<?php if(get_field('landing_cost_title')) : ?>
	<?php the_field('landing_cost_title'); ?>
<?php endif; ?>

<?php if( have_rows('landing_cost_repeater') ) : ?>
	<?php while( have_rows('landing_cost_repeater') ) : the_row(); ?>

		<p><?php the_sub_field('landing_cost_repeater_title'); ?></p>
		<p><?php the_sub_field('landing_cost_repeater_heading'); ?></p>
		<p><?php the_sub_field('landing_cost_repeater_text'); ?></p>
		<p><?php the_sub_field('landing_cost_repeater_price'); ?></p>
		<p><?php the_sub_field('landing_cost_repeater_currency'); ?></p>

	<?php endwhile; ?>
<?php endif; ?>