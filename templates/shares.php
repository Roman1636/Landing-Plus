<?php if(get_field('landing_shares_title')) : ?>
	<?php the_field('landing_shares_title'); ?>
<?php endif; ?>

<?php if( have_rows('landing_shares_repeater') ) : ?>
	<?php while( have_rows('landing_shares_repeater') ) : the_row(); ?>

		<p><?php the_sub_field('landing_shares_repeater_content'); ?></p>

	<?php endwhile; ?>
<?php endif; ?>