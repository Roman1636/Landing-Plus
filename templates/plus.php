
<?php if(get_field('landing_plus_title')) : ?>
	<?php the_field('landing_plus_title'); ?>
<?php endif; ?>

<?php if( have_rows('landing_plus_repeater') ) : ?>
	<?php while( have_rows('landing_plus_repeater') ) : the_row(); ?>

		<p><?php the_sub_field('landing_plus_repeater_title'); ?></p>
		<p><?php the_sub_field('landing_plus_repeater_text'); ?></p>
		<img width="120px" src="<?php the_sub_field('landing_plus_repeater_img'); ?>">

	<?php endwhile; ?>
<?php endif; ?>

<?php if(get_field('landing_plus_text')) : ?>
	<?php the_field('landing_plus_text'); ?>
<?php endif; ?>

<?php if(get_field('landing_plus_phone')) : ?>
	<?php the_field('landing_plus_phone'); ?>
<?php endif; ?>

<?php if(get_field('landing_plus_img')) : ?>
	<img src="<?php the_field('landing_plus_img'); ?>">
<?php endif; ?>