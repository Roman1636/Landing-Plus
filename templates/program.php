
<?php if(get_field('landing_program_title')) : ?>
	<?php the_field('landing_program_title'); ?>
<?php endif; ?>

<?php if( have_rows('landing_program_repeater') ) : ?>
	<?php while( have_rows('landing_program_repeater') ) : the_row(); ?>

		<p><?php the_sub_field('landing_program_repeater_title'); ?></p>
		<p><?php the_sub_field('landing_program_repeater_duration_title'); ?></p>
		<p><?php the_sub_field('landing_program_repeater_duration_month'); ?></p>
		<p><?php the_sub_field('landing_program_repeater_duration_clock'); ?></p>
		<p><?php the_sub_field('landing_program_repeater_text'); ?></p>

	<?php endwhile; ?>
<?php endif; ?>

<?php if(get_field('landing_program_text')) : ?>
	<?php the_field('landing_program_text'); ?>
<?php endif; ?>

<?php if(get_field('landing_program_footer')) : ?>
	<?php the_field('landing_program_footer'); ?>
<?php endif; ?>